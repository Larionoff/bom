function printKeyValueLI(key,value)
{
  if (value==null || value=='' || value=='unspecified') value = 'не указано';
  document.write('<li>'+key+': '+value+'</li>');
}

function niceProtocolName(protocol)
{
  if (protocol[protocol.length-1]==':') protocol = protocol.substr(0,protocol.length-1);
  return protocol.toUpperCase();
}

function niceParamList(search)
{
  if (search=='') return search;
  if (search[0]=='?') search = search.substr(1);
  var params = search.split('&');
  var html = '';
  html += '<ul>';
  params.forEach(function(param)
  {
    var data = param.split('=',2);
    html += '<li>' + data[0] + ' = ' + data[1] + '</li>';
  });
  html += '</ul>';
  return html;
}

function printBrowserInfo()
{
  document.open();
  document.write('<ul>');
  printKeyValueLI('Браузер',navigator.appName);
  printKeyValueLI('Кодовое название',navigator.appCodeName);
  printKeyValueLI('Версия браузера',navigator.appVersion);
  if (navigator.buildID!=undefined)
  {
    printKeyValueLI('Номер сборки',navigator.buildID);
  }
  printKeyValueLI('Cookie',navigator.cookieEnabled ? 'включено' : 'отключено');
  printKeyValueLI('Настройки do-not-track',navigator.doNotTrack);

  if (navigator.languages!=undefined)
  {
    printKeyValueLI('Языки',navigator.languages);
  } else {
    printKeyValueLI('Язык', navigator.language);
  }
  printKeyValueLI('Подключение к сети',navigator.onLine ? 'подключено' : 'отсутствует');
  printKeyValueLI('Платформа браузера',navigator.platform);
  printKeyValueLI('User-Agent',navigator.userAgent);
  printKeyValueLI('Производитель',navigator.vendor);
  printKeyValueLI('Поддержка Java',navigator.javaEnabled() ? 'включена' : 'выключена');
  document.write('</ul>');
  document.close();
}

function printUrlInfo()
{
  document.open();
  document.write('<ul>');
  printKeyValueLI('Протокол',niceProtocolName(location.protocol));
  printKeyValueLI('Домен',location.hostname);
  printKeyValueLI('Порт',location.port);
  printKeyValueLI('Путь',location.pathname);
  printKeyValueLI('Параметры GET-запроса',niceParamList(location.search));
  printKeyValueLI('Якорь',location.hash);
  document.write('</ul>');
  document.close();
}